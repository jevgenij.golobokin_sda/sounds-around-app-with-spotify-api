package com.eugenegolobokin.app;



import com.eugenegolobokin.app.utils.Config;

import java.io.IOException;

public class Main {


    public static void main(String[] args) throws IOException, InterruptedException {

        /**
         * Setting url endpoints, links and page count on startup
         */
        Config.setConfig(args);

        AdvisorApp app = new AdvisorApp();
        app.run();

    }
}