package com.eugenegolobokin.app.network;

import com.eugenegolobokin.app.model.User;
import com.eugenegolobokin.app.utils.Config;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class RequestHandler {

    public String requestData(User user, String endpointURL) {

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .header("Authorization", "Bearer " + user.getAccessToken())
                .uri(URI.create(Config.API_SERVER_PATH + endpointURL))
                .GET()
                .build();
        HttpResponse<String> response = null;
        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());

        } catch (IOException | InterruptedException ex) {
            ex.printStackTrace();
        }
        return response.body();
    }
}
