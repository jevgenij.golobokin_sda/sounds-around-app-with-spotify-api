package com.eugenegolobokin.app.utils;

import java.util.Scanner;

public class ConsoleInputHandler {

    private static Scanner scanner = new Scanner(System.in);

    public static String getUserInputLine() {
        return scanner.nextLine();
    }


}
