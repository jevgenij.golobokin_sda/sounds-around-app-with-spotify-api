package com.eugenegolobokin.app.utils;

import com.eugenegolobokin.app.model.User;
import com.eugenegolobokin.app.model.spotify.Data;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.net.http.HttpResponse;

public class JsonResponseParser {

    Gson gson = new GsonBuilder().setPrettyPrinting().create();


    public void parseTokenData(HttpResponse<String> response, User user) {
        JsonObject userAuthData = JsonParser.parseString(response.body()).getAsJsonObject();

        if (userAuthData != null) {
            user.setAuth(true);
            user.setAccessToken(userAuthData.get("access_token").getAsString());
            user.setTokenType(userAuthData.get("token_type").getAsString());
            user.setScope(userAuthData.get("scope").getAsString());
            user.setExpiresIn(userAuthData.get("expires_in").getAsInt());
            user.setRefreshToken(userAuthData.get("refresh_token").getAsString());
        }
    }

    //TODO rewrite parsing to User.class with Gson
    /*
    public void parseTokenData2(HttpResponse<String> response, User user) {
        JsonObject userAuthData = com.google.gson.JsonParser.parseString(response.body()).getAsJsonObject();

        if (userAuthData != null) {
            user.setAuth(true);
            gson.fromJson(response, User.class)
        }
    }
    */


    public Data parseData(String responseJson) {
        return gson.fromJson(responseJson, Data.class);
    }

}
