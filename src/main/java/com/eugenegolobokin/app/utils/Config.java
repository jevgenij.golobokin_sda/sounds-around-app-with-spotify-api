package com.eugenegolobokin.app.utils;



import com.eugenegolobokin.app.model.User;

import java.util.Base64;

public class Config {
    private static final String AUTH_SERVER_PATH_DEFAULT = "https://accounts.spotify.com";
    private static final String API_SERVER_PATH_DEFAULT = "https://api.spotify.com";

    public static final String REDIRECT_URI = "http://localhost:8080";
    public static final String REQ_ACCESS_TOKEN_ENDPOINT = "/api/token";

    private static final String CLIENT_ID = "9b9e495f86a445ed9b40bac09f3a5d2e";
    private static final String CLIENT_SECRET = "9fdcf9c69f8a4d888779e22a76b77a6c";

    // endpoints for requests
    public static final String FEATURED_ENDPOINT_URL = "/v1/browse/featured-playlists";
    public static final String NEW_ENDPOINT_URL = "/v1/browse/new-releases";
    public static final String CATEGORIES_ENDPOINT_URL = "/v1/browse/categories";
    public static final String PLAYLIST_ENDPOINT_URL = "/v1/browse/categories/%s/playlists";


    public static String AUTH_SERVER_PATH;
    public static String API_SERVER_PATH;

    public static String AUTH_ACCOUNT_PATH_FULL;
    public static String REQUEST_ACCESS_TOKEN_PATH_FULL;

    public static int ITEMS_PER_PAGE = 5;

    public static void setConfig(String[] args) {

        for (int i = 0; i < args.length; i += 2) {
            switch (args[i]) {
                case "-access":
                    AUTH_SERVER_PATH = args[i + 1];
                    break;
                case "-resource":
                    API_SERVER_PATH = args[i + 1];
                    break;
                case "-page":
                    ITEMS_PER_PAGE = Integer.parseInt(args[i + 1]);
                    break;
            }
        }

        checkPaths();
        setAuthAccountPath();

    }

    public static void setRequestAccessTokenPath(User user) {
        REQUEST_ACCESS_TOKEN_PATH_FULL =
                String.format(
                        "grant_type=authorization_code&code=%s&redirect_uri=%s&client_id=%s&client_secret=%s",
                        user.getCode(), REDIRECT_URI, CLIENT_ID, CLIENT_SECRET
                );
    }

    private static void setAuthAccountPath() {
        AUTH_ACCOUNT_PATH_FULL =
                AUTH_SERVER_PATH +
                        "/authorize?client_id=" +
                        CLIENT_ID +
                        "&redirect_uri=" +
                        REDIRECT_URI +
                        "&response_type=code";
    }

    private static void checkPaths() {
        if (AUTH_SERVER_PATH == null || AUTH_SERVER_PATH.isEmpty()) {
            AUTH_SERVER_PATH = AUTH_SERVER_PATH_DEFAULT;
        }
        if (API_SERVER_PATH == null || API_SERVER_PATH.isEmpty()) {
            API_SERVER_PATH = API_SERVER_PATH_DEFAULT;
        }
    }

    private static String encodeToBase64(String toEncode) {
        return Base64.getEncoder().encodeToString(toEncode.getBytes());
    }

    public static String getEncodedClientKeys() {
        return encodeToBase64(CLIENT_ID + ":" + CLIENT_SECRET);
    }

    public static String compilePlaylistUrl(String categoryId) {
        return String.format(PLAYLIST_ENDPOINT_URL, categoryId);
    }
}
