package com.eugenegolobokin.app;

import com.eugenegolobokin.app.model.User;
import com.eugenegolobokin.app.model.spotify.Categories;
import com.eugenegolobokin.app.model.spotify.Data;
import com.eugenegolobokin.app.network.RequestHandler;
import com.eugenegolobokin.app.utils.Config;
import com.eugenegolobokin.app.utils.JsonResponseParser;
import com.eugenegolobokin.app.view.PagePrinter;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Scanner;

public class AdvisorApp {

    User user = new User();
    private Categories categories;

    RequestHandler requestHandler = new RequestHandler();
    JsonResponseParser jsonResponseParser = new JsonResponseParser();
    PagePrinter pagePrinter;

    public void run() throws IOException, InterruptedException {

        Scanner scanner = new Scanner(System.in);

        String userInput = "";
        boolean authorized = false;

        System.out.println("WELCOME TO SPOTIFY GETTER APP!");

        while (!authorized && scanner.hasNext()) {
            userInput = scanner.nextLine().trim();
            //TODO replace check with user.isAuth
            if (userInput.equals("auth")) {

                System.out.println("use this link to request the access code:");
                System.out.println(Config.AUTH_ACCOUNT_PATH_FULL);
                httpServerListen();

                System.out.println("making http request for access_token...");
                requestAccessToken();

                System.out.println("Success!");


                authorized = true;
                break;
            } else if (userInput.equals("exit")) {
                System.out.println("---GOODBYE!---");
                break;
            } else {
                System.out.println("Please, provide access for application.");
            }

        }


        while (authorized) {
            String response;
            userInput = scanner.next();
            switch (userInput) {
                case "featured":
                    //TODO extract to method
                    response = requestHandler.requestData(user, Config.FEATURED_ENDPOINT_URL);
                    Data featuredPlaylists = jsonResponseParser.parseData(response);
                    featuredPlaylists.getPlaylists().printPlaylist();
                    break;
                case "new":
                    //TODO extract to method
                    response = requestHandler.requestData(user, Config.NEW_ENDPOINT_URL);
                    Data newAlbums = jsonResponseParser.parseData(response);
                    newAlbums.getAlbums().printAlbums();
                    break;
                case "categories":
                    setCategories();
                    categories.printCategories();
                    break;
                case "playlists":
                    String category = scanner.nextLine().trim();
                    //TODO check if Categories are initialized!
                    setCategories();
                    String categoryId = categories.getCategoryIdByName(category);

                    // TODO remove this after proper check with json object is implemented
                    if (categoryId.equals("bad")) {
                        System.out.println("Unknown category name.");
                        break;
                    }

                    response = requestHandler.requestData(user, Config.compilePlaylistUrl(categoryId));

                    //TODO proper check with this
                    JsonObject jo = JsonParser.parseString(response).getAsJsonObject();
                    if (jo.toString().contains("error")) {
                        System.out.println(jo.getAsJsonObject("error").get("message"));
                    } else {
                        Data categoryPlaylists = jsonResponseParser.parseData(response);
                        categoryPlaylists.getPlaylists().printPlaylist();
                    }


                    break;
                case "exit":
                    System.out.println("---GOODBYE!---");
                    authorized = false;
                    break;
            }
        }
    }

    private void setCategories() {
        String response = requestHandler.requestData(user, Config.CATEGORIES_ENDPOINT_URL);
        Data categories = jsonResponseParser.parseData(response);
        this.categories = categories.getCategories();
    }

    //TODO move method to network package
    public String requestAccessToken() throws IOException, InterruptedException {

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .headers("Content-Type", "application/x-www-form-urlencoded",
                        "Authorization", "Basic " + Config.getEncodedClientKeys())
                .uri(URI.create(Config.AUTH_SERVER_PATH + Config.REQ_ACCESS_TOKEN_ENDPOINT))
                .POST(HttpRequest.BodyPublishers.ofString(Config.REQUEST_ACCESS_TOKEN_PATH_FULL))
                .build();


        HttpResponse<String> response = client.send(request,
                HttpResponse.BodyHandlers.ofString());

        // System.out.println(response.toString());

        //TODO take parsing out of this method
        new JsonResponseParser().parseTokenData(response, user);

        return response.body();
    }

    //TODO move method to network package
    public void httpServerListen() throws IOException, InterruptedException {

        HttpServer server = HttpServer.create();
        server.bind(new InetSocketAddress(8080), 0);
        server.start();

        server.createContext("/",
                exchange -> {
                    String query = exchange.getRequestURI().getQuery();

                    String result;

                    if (query != null && query.contains("code")) {
                        user.setCode(query.substring(5));
                        Config.setRequestAccessTokenPath(user);
                        result = "Got the code. Return back to your program.";
                    } else {
                        result = "Not found authorization code. Try again.";
                    }

                    exchange.sendResponseHeaders(200, result.length());
                    exchange.getResponseBody().write(result.getBytes());
                    exchange.getResponseBody().close();

                    System.out.println(result);
                }
        );
        System.out.println("waiting for code...");

        //TODO with boolean check!
        while (user.getCode().isBlank()) { // == ""
            Thread.sleep(10);
        }

        server.stop(10);
    }


}




