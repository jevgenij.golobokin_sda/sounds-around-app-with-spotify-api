package com.eugenegolobokin.app.view;

import com.eugenegolobokin.app.utils.Config;
import com.eugenegolobokin.app.utils.ConsoleInputHandler;

import java.util.Set;

public class PagePrinter<T> {
    private Set<T> items;
    private long offset = 0;
    private int currentPage = 1;
    private int totalPages;


    public void print(Set<T> items) {
        this.items = items;
        totalPages = (items.size() / Config.ITEMS_PER_PAGE)
                + (items.size() % Config.ITEMS_PER_PAGE != 0 ? 1 : 0);
        printPage();
        boolean isPrinting = true;

        while (isPrinting) {
            String userInput = ConsoleInputHandler.getUserInputLine();
            switch (userInput) {
                case "next":
                    printNextPage();
                    break;
                case "prev":
                    printPrevPage();
                    break;
                default:
                    isPrinting = false;
                    //TODO proper return to main app menu
            }
        }
    }

    public void printNextPage() {
        if (currentPage >= totalPages) {
            System.out.println("No more pages.");
        } else {
            offset += Config.ITEMS_PER_PAGE;
            currentPage += 1;
            printPage();
        }
    }

    public void printPrevPage() {
        if (currentPage == 1) {
            System.out.println("No more pages.");
        } else {
            offset -= Config.ITEMS_PER_PAGE;
            currentPage -= 1;
            printPage();
        }
    }
    public void printPage() {
        items.stream()
                .skip(offset)
                .limit(Config.ITEMS_PER_PAGE)
                .forEach(System.out::println);
        System.out.printf("---PAGE %d OF %d---%n", currentPage, totalPages);
    }
}
