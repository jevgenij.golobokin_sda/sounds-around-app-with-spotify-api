package com.eugenegolobokin.app.model.spotify;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;

public class Album {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("artists")
    private Artist[] artists;

    public String getName() {
        return name;
    }

    public Artist[] getArtists() {
        return artists;
    }

    public String getAlbumURL() {
        return "https://open.spotify.com/album/" + id;
    }

    @Override
    public String toString() {
        return String.format(
                "%s%n%s%n%s%n",
                name,
                Arrays.toString(artists),
                getAlbumURL()
        );
    }
}
