package com.eugenegolobokin.app.model.spotify;

import com.google.gson.annotations.SerializedName;

public class Playlist {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;

    public String getName() {
        return name;
    }

    public String getAlbumURL() {
        return "https://open.spotify.com/playlist/" + id;
    }

    @Override
    public String toString() {
        return String.format(
                "%s%n%s%n",
                name,
                getAlbumURL()
        );
    }
}
