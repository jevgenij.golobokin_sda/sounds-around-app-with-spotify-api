package com.eugenegolobokin.app.model.spotify;

import com.eugenegolobokin.app.view.PagePrinter;
import com.google.gson.annotations.SerializedName;

import java.util.Set;

public class Playlists {
    @SerializedName("items")
    private Set<Playlist> playlists;

    public Set<Playlist> getPlaylists() {
        return playlists;
    }

    public void printPlaylist() {
        //playlists.forEach(System.out::println);
        new PagePrinter().print(playlists);
    }
}
