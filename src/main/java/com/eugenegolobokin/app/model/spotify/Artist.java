package com.eugenegolobokin.app.model.spotify;

import com.google.gson.annotations.SerializedName;

public class Artist {
    @SerializedName("name")
    private String name;

    @Override
    public String toString() {
        return name;
    }
}
