package com.eugenegolobokin.app.model.spotify;

import com.eugenegolobokin.app.view.PagePrinter;
import com.google.gson.annotations.SerializedName;

import java.util.Set;

public class Categories {
    @SerializedName("items")
    private Set<Category> categories;

    public Set<Category> getCategories() {
        return categories;
    }

    public String getCategoryIdByName(String name) {

        String id = "bad";
        for (Category cat : categories) {
            if (cat.getName().equalsIgnoreCase(name)) {
                id = cat.getId();
                break;
            }
        }

        // TODO proper check of id existence
//        return categories.stream()
//                .filter(c -> c.getName().equalsIgnoreCase(name))
//                .map(Category::getId)
//                .findAny()
//                .orElse("37i9dQZF1DXcBWIGoYBM5M");

        return id;
    }

    public void printCategories() {
        //categories.forEach(System.out::println);
        new PagePrinter().print(categories);
    }
}
