package com.eugenegolobokin.app.model.spotify;

import com.google.gson.annotations.SerializedName;

public class Data {
    @SerializedName("albums")
    private Albums albums;

    @SerializedName("categories")
    private Categories categories;

    @SerializedName("playlists")
    private Playlists playlists;

    public Albums getAlbums() {
        return albums;
    }

    public Categories getCategories() {
        return categories;
    }

    public Playlists getPlaylists() {
        return playlists;
    }
}
