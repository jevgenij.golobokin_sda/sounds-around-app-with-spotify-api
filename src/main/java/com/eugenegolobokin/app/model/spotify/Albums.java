package com.eugenegolobokin.app.model.spotify;

import com.eugenegolobokin.app.view.PagePrinter;
import com.google.gson.annotations.SerializedName;

import java.util.Set;

public class Albums {
    @SerializedName("items")
    private Set<Album> albums;

    public Set<Album> getAlbums() {
        return albums;
    }

    public void printAlbums() {
        //albums.forEach(System.out::println);
        new PagePrinter().print(albums);
    }
}
