# SPOTIFY API app

### About the app

**//IN PROGRESS**
<br/>App connects to Spotify API with user authorization using local server. After that you can request info on albums and playlists. 
<br/>I plan to add functionalities one by one

*Now you can:*
1. `auth` command returns spotify user authorization link;
2. after user authorization you can get:
    <br/>`new` - new playlists
    <br/>`featured` - featured playlists
    <br/>`categories` - list of playlist categories
    <br/>`playlists C_NAME` (where C_NAME is category name) - playlists of selected category
3. `next` or `prev` - navigate pages
4. `exit` - exit app

### Demo

![]()

### How to run
This is a Maven project with console UI
1. Clone or download project
2. Run Main.java

### Tech used
*Java 13, Apache Maven, Google gson 2.8.6, Spotify API*

## Implementing
*HttpServer, HttpRequest, HttpResponse, MVC pattern, GsonBuilder, JsonParser, Generics*

